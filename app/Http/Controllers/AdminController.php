<?php

namespace App\Http\Controllers;

use DB;
use App\Home;
use App\User;
use App\Admin;
use App\Order;
use App\Member;
use App\Product;
use Carbon\Carbon;

use App\Category;   
use App\OrderProducts;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){  
        
        $date = \Carbon\Carbon::today()->subDays(7);

        $categories = Category::all();
        $userCount = DB::table('users')->count();
        $salesCount = DB::table('order_product')->sum('quantity');
        $totalRevenue = DB::table('orders')->sum('billing_total');
        $salesInSevenDays = DB::table('order_product')->where('created_at', '>=', $date)->count();
        $revenueInSevenDays = DB::table('orders')->where('created_at', '>=', $date)->sum('billing_total');

        
        // $RevenueInSevenDays = 
        // $categoryName = 'Featured';
        return view('admin.index', [ 'categories' => $categories, 
                                    'userCount' => $userCount, 
                                    'salesCount' => $salesCount, 
                                    'totalRevenue' => $totalRevenue,
                                    'salesInSevenDays' => $salesInSevenDays, 
                                    'revenueInSevenDays' => $revenueInSevenDays,
                                    'mode' => 'admin']);

    }
    public function memberList(){
        $memberList = DB::table('users')->get();
        $categories = Category::all();
        return view('admin.memberList', ['categories' => $categories, 'memberList' => $memberList, 'mode' => 'admin']);
    }
    public function gamelist(){

        $categories = Category::all();
        $home = Product::with('categories')->get();
        return view('admin.gamelist', ['categories' => $categories, 'home' => $home, 'mode' => 'admin']);
    }
}
