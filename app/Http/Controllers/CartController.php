<?php

namespace App\Http\Controllers;

use DB;
use App\Product;
use App\Home;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function public()
    {


    }


    public function index()
    {
        $categories = DB::table('categories')->get();
        $products = DB::table('products')->get();


                 
        return view('cart.index', ['products'=>$products,'cart' => null,'categories' => $categories, 'mode' => 'public']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       

        $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });

        if ($duplicates->isNotEmpty()) {
            return redirect()->route('cart.index')->with('success_message', "L'article est déjà dans votre panier");
        }

        Cart::add($request->get('id'), $request->get('name'), 1,$request->get('price'))->associate('App\Product');

            return redirect()->route('cart.index')->with('success_message',"L'article a été ajouté à votre panier!");
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        // if ($validator->fails()) {
        //     session()->flash('errors', collect(['la quantité doit être comprise entre 1 et 5']));
        //     return response()->json(['success' => false], 400);  // 400 pour bad request
        // }
        
        if($request->quantity > $request->productQuantity) {
            session()->flash('errors', collect(["Nous n'avons actuellement pas assez de produits en stock"]));
            return response()->json(['success' => false], 400);  // 400 pour bad request
        }
        
        
        Cart::update($id, $request->quantity);

        session()->flash('success_message','La quantité a été mise à jour avec succès !');
        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);

        return back()->with('success_message', "L'article a été supprimé");
    }
}
