<?php

namespace App\Http\Controllers;
use DB;
use App\Order;
use App\Coupon;
use App\Product;

use App\OrderProduct;
use App\Mail\OrderPlaced;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Gloudemans\Shoppingcart\Facades\Cart;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = DB::table('categories')->get();
        $discount = session()->get('coupon')['discount'] ?? 0;
        $newSubtotal = (Cart::subtotal() - $discount);
        $newTotal = $newSubtotal;



        return view('checkout.index')->with([
            'discount' => $discount,
            'newSubtotal' => $newSubtotal,
            'newTotal' => $newTotal,
            'categories' => $categories,
            'mode' => 'public'
        ]);
        // return view('checkout.index' ,['discount' => '$discount', 'newSubtotal' => '$newSubtotal', 'newTotal' => '$newTotal','categories' => $categories, 'mode' => 'public']);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Vérifiez les disponibilité lorsqu'il y a moins d'articles disponibles à l'achat
        if ($this->productsAreNoLongerAvailable()){
            return back()->withErrors("Désolé ! Un des produits dans votre panier n'est plus disponible ");
        }
        // if ($this->productsAreNoLongerAvailable()) {
        //     return back()->withErrors("Désolé! L'un des articles de votre panier n'est plus disponible.");
        // }

        //Insert into orders table


        $order = $this->addToOrdersTables($request,null);
        
        Mail::send(new OrderPlaced($order));


        // Diminuer les quantités de tout les produits du panier

        $this->decreaseQuantities();



        Cart::instance('default')->destroy();
        session()->forget('coupon');
        
        return redirect()->route('confirmation.index')->with('success_message', 'Merci! Votre paiement a été accepté avec succès!');
    }

    private function getNumbers()
    {
        $discount = session()->get('coupon')['discount'] ?? 0;
        $code = session()->get('coupon')['name'] ?? null;
        $newSubtotal = (Cart::subtotal() - $discount);
        $newTotal = $newSubtotal;

        return collect([
            'discount' => $discount,
            'code' => $code,
            'newSubtotal' => $newSubtotal,
            'newTotal' => $newTotal,
        ]);
    }

    protected function addToOrdersTables($request, $error)
    {
        $order = Order::create([
            'user_id' => auth()->user() ? auth()->user()->id : null,
            'billing_email' => $request->email,
            'billing_name' => $request->name,
            'billing_address' => $request->address,
            'billing_city' => $request->city,
            'billing_postalcode' => $request->postalcode,
            'billing_phone' => $request->phone,
            'billing_name_on_card' => $request->name_on_card,
            'billing_discount' => $this->getNumbers()->get('discount'),
            'billing_discount_code' => $this->getNumbers()->get('code'),
            'billing_total' => $this->getNumbers()->get('newTotal'),
        ]);

        // Insert into order-product table
        foreach (Cart::content() as $item) {
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $item->model->id,
                'quantity' => $item->qty,
            ]);
        }

        return $order;
    }
    
    protected function decreaseQuantities()
    {
        foreach (Cart::content() as $item) {
            $products = Product::find($item->model->id);

            $products->update(['quantity' => $products->quantity - $item->qty]);
        }
    }

    protected function productsAreNoLongerAvailable()
    {
        foreach (Cart::content() as $item) {
            $products = Product::find($item->model->id);

            if ($products->quantity < $item->qty) {
                return true;
            }
        }

        return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
