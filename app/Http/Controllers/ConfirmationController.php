<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\User;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
// use App\Product;


class ConfirmationController extends Controller
{
    public function index()
    {
        $categories = Category::all();
       
         if (! session()->has('success_message')) {
               
            // $member = User::find(Auth::id());
            // Mail::to($member->email)->send(new InvoiceMail());
            return redirect ('/shop');
            // return redirect('/');
         }
            return view ('thankyou.index')->with(['categories' => $categories, 'mode' => 'public']);
    }
}
