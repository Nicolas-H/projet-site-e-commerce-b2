<?php

namespace App\Http\Controllers;

use App\Product;
use App\Home;
use App\Category;
use Illuminate\Http\Request;
use DB; 

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

// public function __construct(){
//     // $this->middleware('auth');
// }
    public function register(){
        return view('home');
    }
     
    public function public(){

        $pagination = 8;
        $categories = Category::all();
        
        if (request()->category) {
            $home = Product::where('category_id', request()->category)->paginate($pagination);
            
            $categoryName = optional($categories->where('id', request()->category)->first())->name;
        } 
        else {
        
        $home = Product::where('featured', true)->paginate($pagination);
        $categoryName = 'En vedette';
    
        }
        return view('home.index')->with([
            'home' => $home,
            'categories' => $categories,
            'categoryName' => $categoryName,
            'mode' => 'public'
        ]);  
    }
    public function index()
    {
        $home = DB::table('products')->get();
        return view('admin.home', ['home' => $home, 'mode' => 'admin']);
    }

    public function show($slug)
    {
    
        $categories = DB::table('categories')->get();
        $products = Product::where('slug',$slug)->firstOrFail();

        if ($products->quantity > 5){
            $stockLevel = '<div class="badge badge-success">En Stock</div>';
        } elseif ($products->quantity > 0){
            $stockLevel = '<div class="badge badge-warning">Faible sotck</div>';
        } else {
            $stockLevel = '<div class="badge badge-danger">Indisponible</div>';
        }
        

        return view('products.index',['products' => $products,'categories' => $categories,'stockLevel' => $stockLevel, 'mode' => 'public']);
    }
    

}
