<?php

namespace App\Http\Controllers;

use App\KeyProduct;
use Illuminate\Http\Request;

class KeyProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KeyProduct  $keyProduct
     * @return \Illuminate\Http\Response
     */
    public function show(KeyProduct $keyProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KeyProduct  $keyProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(KeyProduct $keyProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KeyProduct  $keyProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KeyProduct $keyProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KeyProduct  $keyProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(KeyProduct $keyProduct)
    {
        //
    }
}
