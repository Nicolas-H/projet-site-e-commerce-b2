<?php

namespace App\Http\Controllers;
use Auth;
use App\Member;
use App\Category;
use App\Product;
use App\User;
use Illuminate\Http\Request;


use DB;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {
        
        $member = User::find(Auth::id());
        $categories = Category::all();
        return view('members.index', ['member' => $member, 'categories' => $categories, 'mode' => 'member']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $inputs = $request->except('_token');
        $path = basename($inputs['image']->store('public/img/members'));
        $member = new User();
    
        foreach ($inputs as $key => $value) {
            $member->$key = $value;
        }

        $member->image=$path;
        $member->save();
        return view('members.index', ['members' => $members, 'member' => $member, 'categories' => $categories, 'mode' => 'member']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = User::find($id);
        // $members = User::all();
        $categories = Category::all();
        return view('members', ['member' => $member, 'categories' => $categories, 'mode' => 'member']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $inputs = $request->except('_token', '_method');
        // $this->validate($request, [
        //     'select_file' => 'required|image'
        // ]);
        $path = basename($inputs['image']->store('public/img/members'));
        $member = User::find($id);
        foreach($inputs as $key => $value){
            $member->$key = $value;
        }
        $member->image=$path;
        $member->save();
        return redirect('members');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = User::find($id);
        $member->delete($id);
        return redirect('members');
    }
}
