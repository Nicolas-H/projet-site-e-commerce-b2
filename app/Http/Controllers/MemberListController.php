<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Category;   
use App\Home;
use App\Product;
use App\User;
use App\Member;
use DB;
class MemberListController extends Controller
{

    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        
        $memberList = DB::table('users')->get();
        $count = DB::table('users')->count();
        $categories = Category::all();
        return view('admin.memberList', ['categories' => $categories, 'memberList' => $memberList, 'count' => $count, 'mode' => 'admin']);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $memberList = User::find($id);
        $categories = Category::all();
        return view('memberList.edit', ['memberList' => $memberList, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->except('_token', '_method');
        $memberList = User::find($id);
        foreach($inputs as $key => $value){
            $memberList->$key = $value;
        }
        $memberList->save();
        return redirect('admin/memberList');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $memberList = User::find($id);
        $memberList->delete();
        return redirect('admin/memberList');
    }
}
