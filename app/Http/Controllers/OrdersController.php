<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Order;
use App\Category;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $member = User::find(Auth::id());
        $categories = Category::all();

        $orders = auth()->user()->orders()->with('products')->get();




        return view('my-orders.index', ['orders' => $orders, 'member' => $member, 'categories' => $categories, 'mode' => 'member']);
    }

    public function getPdf($id)
    {
        // if (auth()->id() != $order->user_id) {
        //     return back()->withError("Vous n'avez pas accès à cette commande");
        // }
        $order = Order::find($id);
        $products = $order->products;
        $member = User::find(Auth::id());
        $categories = Category::all();
        $data = [
            "order" => $order,
            "products" => $products,
            "member" => $member,
            "categories" => $categories,
        ];
        // $pdf = PDF::loadView('components/my-order', compact('order','products','member','categories'));
        $pdf = PDF::loadView('invoice/invoice', $data);
        return $pdf->download('invoice.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        if (auth()->id() != $order->user_id) {
            return back()->withError("Vous n'avez pas accès à cette commande");
        }
        $products = $order->products;
        $member = User::find(Auth::id());
        $categories = Category::all();
        return view('my-order.index')->with([
            'order' => $order,
            'products' => $products,
            'member' => $member, 
            'categories' => $categories,
        ]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
