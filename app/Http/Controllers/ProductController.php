<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Home;
use DB;
use App\User;
use Illuminate\Http\Request;
use App\KeyProduct;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function public () {

        $products = DB::table('products')->get();
        return view('products.index', ['homes' => $home,'products' => $products, 'mode' => 'public']);
    }

    // public function index()
    // {
    //     $home = DB::table('products')->get();
    //     return view('admin.products', ['homes' => $home, 'products' => $products, 'mode' => 'admin']);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       
        // $quantity = $inputs['quantity'];
        // factory(App\KeyProduct::class, $quantity)->create();

        $categories = Category::all();
        return view('products.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = $request->except('_token');
        // $quantity = $inputs['quantity'];
        // factory(KeyProduct::class, $quantity)->create();


        $path = basename($inputs['image']->store('public/img/products'));
        
        $home = new Product();
        
        foreach ($inputs as $key => $value) {
            $home->$key = $value;
        }

        $home->image=$path;
        $home->save();
        return redirect('admin/gamelist');
    }



    

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $product = Product::find($id);
        return view('products.edit', ['product' => $product, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $inputs = $request->except('_token', '_method');
        $path = basename($inputs['image']->store('public/img/products'));
        $product = Product::find($id);
        foreach ($inputs as $key => $value) {
            $product->$key = $value;
        }

       
        $product->image=$path;
        $product->save();
        return redirect('admin/gamelist');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $home = Product::find($id);
        $home->delete($id);
        return redirect('admin/gamelist');
    }

    
}
