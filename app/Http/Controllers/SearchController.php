<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;
use App\Product;

class SearchController extends Controller
{
    public function search(Request $request){

        $request->validate([
            'query' => 'required|min:3',
        ]);
        $query = $request->input('query');
        $categories = Category::all();
        $searchResult = Product::where('name', 'like', "%$query%")
                                ->orWhere('description', 'like', "%$query%")
                                ->with(["categories"])
                                ->paginate(5);
        return view('searchResult.index')->with(['categories' => $categories, 'searchResult' => $searchResult, 'mode' => 'public']);
    }

    public function show($slug)
    {
        $categories = DB::table('categories')->get();
        $searchResult = Product::where('slug',$slug)->firstOrFail();
        return view('shop.show',['categories' => $categories, 'searchResult' => $searchResult, 'mode' => 'public']);
    }
}
