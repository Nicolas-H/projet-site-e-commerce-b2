<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable= [
        'user_id', 'billing_email', 'billing_name', 'billing_address', 'billing_city', 
        'billing_postalcode', 'billing_phone', 'billing_name_on_card', 'billing_discount',
        'billing_discount_code','billing_total', 
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('quantity');
        return $this->belongsToMany('App\Product', 'order_product', 'product_id', 'order_id')->withPivot(['quantity']);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d-m-Y');
    }

}
