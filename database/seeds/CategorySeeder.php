<?php

use Carbon\Carbon;
use App\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $now = Carbon::now()->toDateTimeString();

        DB::table('categories')->insert(
            array(
                array( 
                    'name' => 'Xbox',
                    'slug' => 'xbox',
                ),
                array(
                    'name' => 'Playstation',
                    'slug' => 'playstation',
                ),
                array(
                    'name' => 'PC',
                    'slug' => 'pc',
                ),
                array(
                    'name' => 'Switch',
                    'slug' => 'switch',
                ),
            )
        );
    }
}
