<?php

use Illuminate\Database\Seeder;

class CouponsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coupons')->insert(
            array(
                array( 
                    'code' => 'ynov',
                    'type' => 'percent',
                    'percent_off' => 5,
                ),
                array( 
                    'code' => 'vip',
                    'type' => 'fixed',
                    'value' => 5,
                ),
            )
        );
    }
}
