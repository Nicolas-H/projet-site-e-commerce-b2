<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            array(
                array(
                    'name' => 'Halo 5',
                    'slug' => 'halo-5',
                    'category_id' => 1,
                    'description' => "Halo 5: Guardians sur Xbox One est un FPS mettant en scène les aventures du Master Chief et d'un nouveau personnage, le Spartan Jameson Locke. ",
                    'releaseDate' => '27 octobre 2015',
                    'price' => '54.99',
                ),
                array(
                    'name' => 'Doom',
                    'slug' => 'doom',
                    'category_id' => 2,
                    'description' => "Exemple doom",
                    'releaseDate' => '28 décembre 2019',
                    'price' => '69.99',
                ),
                array(
                    'name' => 'God of war',
                    'slug' => 'god-of-war',
                    'category_id' => 3,
                    'description' => "god of war c trop bien",
                    'releaseDate' => '15 octobre 1765',
                    'price' => '43.99',
                ),
                array(
                    'name' => 'God of war',
                    'slug' => 'god-of-war',
                    'category_id' => 3,
                    'description' => "god of war c trop bien",
                    'releaseDate' => '15 octobre 1765',
                    'price' => '43.99',
                ),
                array(
                    'name' => 'God of war',
                    'slug' => 'god-of-war',
                    'category_id' => 3,
                    'description' => "god of war c trop bien",
                    'releaseDate' => '15 octobre 1765',
                    'price' => '43.99',
                ),
                array(
                    'name' => 'God of war',
                    'slug' => 'god-of-war',
                    'category_id' => 3,
                    'description' => "god of war c trop bien",
                    'releaseDate' => '15 octobre 1765',
                    'price' => '43.99',
                ),
                array(
                    'name' => 'God of war',
                    'slug' => 'god-of-war',
                    'category_id' => 3,
                    'description' => "god of war c trop bien",
                    'releaseDate' => '15 octobre 1765',
                    'price' => '43.99',
                ),
                array(
                    'name' => 'God of war',
                    'slug' => 'god-of-war',
                    'category_id' => 3,
                    'description' => "god of war c trop bien",
                    'releaseDate' => '15 octobre 1765',
                    'price' => '43.99',
                ),
                array(
                    'name' => 'God of war',
                    'slug' => 'god-of-war',
                    'category_id' => 3,
                    'description' => "god of war c trop bien",
                    'releaseDate' => '15 octobre 1765',
                    'price' => '43.99',
                ),
                array(
                    'name' => 'God of war',
                    'slug' => 'god-of-war',
                    'category_id' => 3,
                    'description' => "god of war c trop bien",
                    'releaseDate' => '15 octobre 1765',
                    'price' => '43.99',
                ),
            )
        );

    }
}

            
//     }
// }

// Product::create([
//     'name' => 'Halo 5',
//     'slug' => 'halo-5',
//     'description' => "Halo 5: Guardians sur Xbox One est un FPS mettant en scène les aventures du Master Chief et d'un nouveau personnage, le Spartan Jameson Locke. ",
//     'releaseDate' => '27 octobre 2015',
//     'price' => '54.99'

// ]);

// Product::create([
//     'name' => 'God of war',
//     'slug' => 'god-of-war',
//     'description' => "god of war c trop bien",
//     'releaseDate' => '15 octobre 1765',
//     'price' => '43.99'

// ]);

// Product::create([
//     'name' => 'Trackmania',
//     'slug' => 'trackmania',
//     'category' => 'PC',
//     'description' => "trackmania stadium",
//     'releaseDate' => '15 octobre 1765',
//     'price' => '25'

// ]);
