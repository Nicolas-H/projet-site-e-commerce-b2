<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            array(
                array(
                    'name' => 'administrator',
                    'lastname' => 'Nico',
                    'image' => 'https://api.adorable.io/avatars/215/abott@adorable.png',
                    'birth' => '2020-12-5',
                    'email' => 'admin@eshop.com',
                    'password' => '$2y$10$/SvS7yvAm0IBVimYuyFISugXlNULhYtk8mqqSUzey/slCRWJRsRu.',
                    'balance' => 0,
                    'is_admin' => 1
                ),
            )
        );
    }
}
