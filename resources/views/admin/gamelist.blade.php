@extends('admin.template')
@section('title', 'Mes jeux')

@section('content')
@component('components.admin.gamelist', ['homes' => $home, 'categories' => $categories, 'mode' => 'admin'])

@endcomponent
@endsection