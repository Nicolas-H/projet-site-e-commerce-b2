@extends('admin.template')
@section('title', 'Dashboard')
@section('content')

<div class="follow">
    <a href="admin/#" style="text-decoration: unset">dashboard <i class="fas fa-chevron-right"></i></a>
    
</div>
<h2>Tableau de bord</h2>

<div class="dashboard-page">
    <div class="dashlist">
        <a class="card" href="admin/gamelist">
            <span><i class="fas fa-list"></i> Liste des jeux</span>
        </a>
        <a class="card" href="admin/memberList">
            <span><i class="fas fa-users"></i> Liste des membres</span>
        </a>
    </div>
    <div class="stats">
        <div class="stats-results"> <span class="title-stats">Nombre total de membres</span>
            <span class="result-stats"><b>{{$userCount}}</b></span></div>
        <div class="stats-results"> <span class="title-stats">Nombre de ventes</span>
            <span class="result-stats"><b>{{$salesCount}}</b></span></div>
        <div class="stats-results"> <span class="title-stats">Nombre de ventes sur les 7 derniers jours</span>
            <span class="result-stats"><b>{{$salesInSevenDays}}</b></span></div>
        <div class="stats-results"> <span class="title-stats">Revenus totaux</span>
            <span class="result-stats"><b>{{$totalRevenue}}</b></span></div>
        <div class="stats-results"> <span class="title-stats">Revenus sur les 7 derniers jours</span>
            <span class="result-stats"><b>{{$revenueInSevenDays}}</b></span></div>
    </div>
</div>



@endsection