@extends('admin.template')
@section('title', 'Mes membres')

@section('content')
@component('components.admin.memberList', ['memberList' => $memberList, 'count' => $count, 'mode' => 'admin'])

@endcomponent
@endsection