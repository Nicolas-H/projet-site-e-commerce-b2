@extends('layouts.template')
@section('title', 'Liste des membres')
@section('content')
@component('components.members', ['member' => $member, 'mode' => 'admin'])
    
@endcomponent
@endsection