@extends('layouts.template')
@section('title', 'Ma fiche produit')

@section('content')
@component('components.products', ['product', 'product' => $products, 'mode' => 'admin'])
    
@endcomponent
@endsection