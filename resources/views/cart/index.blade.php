@extends('layouts.template')
@section('title', 'Cart')
@section('content')
@component('components.carts', ['products'=> $products ,'carts' => $cart, 'categories' => $categories, 'mode' => 'public'])
@endcomponent
@endsection