@extends('layouts.template')
@section('title', 'Checkout')
@section('content')
@component('components.checkout', ['discount' => $discount, 'newSubtotal' => $newSubtotal, 'newTotal' => $newTotal,'categories' => $categories,'mode' => 'public'])
@endcomponent
@endsection