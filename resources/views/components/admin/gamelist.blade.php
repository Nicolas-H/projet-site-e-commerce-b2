<div class="follow mb-3">
  <a href="/admin" style="text-decoration:none">dashboard <i class="fas fa-chevron-right"></i></a>
  <a href="#" style="text-decoration:none">Liste des jeux</a>
</div>
<div class="col">
  <div class="mx-auto mb-2">
    <a class="btn btn-light" href="{{route('product.create')}}" role="button">
      Ajouter un jeu
      <i class="fas fa-plus-circle"></i>
    </a>
  </div>
</div>
<div class="table-responsive-md">
  <table class="table">
  <thead class="bg-dark text-light">
      <th scope="col">id</th>
      <th scope="col">nom</th>
      <th scope="col">categorie</th>
      <th scope="col">prix</th>
      <th scope="col">Quantité</th>

      <th scope="col"></th>
      <th scope="col"></th>
  </thead>
  <tbody>
    @foreach ($homes as $home)
    <tr>
      <td scope="col">{{ $home->id }}</td>
      <td scope="col">{{ $home->name }}</td>
      <td scope="col">{{ $home->categories->name}}</td>
      <td scope="col">{{ $home->price }}€</td>
      <td scope="col">{{ $home->quantity }}</td>

      <td class="d-flex">
        <a class="btn btn-light mr-3" href="{{ route('product.edit', ['product' => $home->id]) }}" role="button">
          <i class="fas fa-pen"></i>
        </a>
        <form method="POST" action="{{ route('product.destroy', ['product' => $home->id]) }}">
          @method('DELETE')
          @csrf
          <button type="submit" class="btn btn-light mt-0">
            <i class="fas fa-trash"></i>
          </button>
        </form>
      </td>
    </tr>
    @endforeach

  </tbody>
  </table>

</div>
