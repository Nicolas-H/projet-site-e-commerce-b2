<div class="follow mb-4">
  <a href="/admin" style="text-decoration:none">dashboard <i class="fas fa-chevron-right"></i></a>
  <a href="#" style="text-decoration:none">Liste des membres</a>
</div>
<h5 class="mb-4">Nombre total de membres : {{$count}}</h5>
<div class="table-responsive">
  <table class="table">

    <thead class="bg-dark text-light">
      <tr>
        <th>id</th>
        <th>nom</th>
        <th>prénom</th>
        <th>email</th>
        <th>Naissance</th>
        <th></th>

      </tr>
    </thead>
    <tbody>
      @foreach ($memberList as $memberList)
      <tr>
        <td>{{ $memberList->id }}</td>
        <td>{{ $memberList->name }}</td>
        <td>{{ $memberList->lastname }}</td>
        <td>{{ $memberList->email }}</td>
        <td>{{$memberList->birth}}</td>
        <td class="d-flex">
          <a class="btn btn-light mr-3"
            href="{{ route('memberList.edit', ['memberList', 'memberList' => $memberList->id]) }}" role="button">
            <i class="fas fa-pen"></i>
          </a>
          <form method="POST"
            action="{{ route('memberList.destroy', ['memberList', 'memberList' => $memberList->id]) }}">
            @method('DELETE')
            @csrf
            <button type="submit" class="btn btn-light mt-0">
              <i class="fas fa-trash"></i>
            </button>
          </form>
        </td>
      </tr>
      @endforeach

    </tbody>
  </table>

</div>