


<div class="cart-page">
  <h1>Mon panier</h1><br>

  @if (session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif

  @if(count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if (Cart::count() > 0)

  <h2>{{Cart::count()}} article(s) dans votre panier</h2>


  @foreach (Cart::content() as $item)

  <div class="cart-details">

    <div class="cart-img">
       <a href="{{ route('shop.show', $item->model->slug)}}"> <img src="{{ asset('/storage/img/products/'.$item->model->image) }}" alt="item"></a>
    </div>

    <div class="card">
      <div class="top-box">
        <div class="title">
        <h2><a href="{{ route('shop.show', $item->model->slug)}}">{{$item->model->name}}</a></h2>
        </div>
        <div class="delete-img">
          
            <form action="{{ route('cart.destroy', $item->rowId) }}" method="POST">
            @csrf
            @method('DELETE')

            <button type="submit" class="cart-options"><img src="/img/delete.png" alt=""></button>
            
            </form>
      
        </div>
      </div>
      <div class="middle-box">
        <div class="synopsis">
        <b>Synopsis :</b> {{$item->model->description}}
        </div>
      </div>
      <div class="bottom-box">
        <div class="cart-release">
        <b>Sortie :</b> {{$item->model->releaseDate}}<br>
        </div>
        <div class="cart-review">
        <b>Note :</b> 3,5/5
      </div>

      </div>
      <div class="cart-price">
        <div>
          <select class="quantity" data-id="{{ $item->rowId }}" data-productQuantity="{{ $item->model->quantity }}">
              @for ($i = 1; $i < 5 + 1 ; $i++)
                  <option {{ $item->qty == $i ? 'selected' : '' }}>{{ $i }}</option>
              @endfor
          </select>
        </div>
        <div class="cart-item-price">
          <span><b>{{$item->subtotal}}€</b></span>
        </div>
      </div> 
    </div>
  </div>

  @endforeach


  <div class="box-total">
    <div class="cart-total">
      <div>
        Sous-total<br>
        Taxes (20%)<br>
        <span class="total"><b>Total</b></span>
      </div>
      <div class="cart-total-subtotal">
        {{ Cart::subtotal()}} €<br>
        {{ Cart::tax()}} €<br>
        <span class=cart-totals-total>{{ Cart::total()}} €</span>
      </div>
    </div> 
  </div>

      <div class="box-payement">
        <a href="{{ route('shop.index') }}" class="btn btn-primary">Continuer vos achats</a> 
        <a href="{{ route('checkout.index') }}" class="btn btn-primary" style="background-color:#ff5400; border-color:#ff5400" >Paiement</a> 
      </div>

    @else

    <h3>Pas d'article dans le panier !</h3>
    <a href="{{ route('shop.index') }}">
      <button type="button" class="btn btn-outline-primary">Continuer vos achats</button>
      </a>

  @endif

</div>

@section('extra-js')
  <script src="{{ asset('js/app.js') }}"></script>
  <script>
    (function(){
      const classname = document.querySelectorAll('.quantity')

      Array.from(classname).forEach(function(element) {
        element.addEventListener('change', function() {
          const id = element.getAttribute('data-id')
          const productQuantity = element.getAttribute('data-productQuantity')

          axios.patch(`/cart/${id}`, {
            quantity: this.value,
            productQuantity: productQuantity

          })
          .then(function (response) {
            // console.log(response);
            window.location.href = '{{ route('cart.index') }}'
          })
          .catch(function (error) {
            // console.log(error);
            window.location.href = '{{ route('cart.index') }}'
          });
        })
      })
    })();
    
  </script>
@endsection