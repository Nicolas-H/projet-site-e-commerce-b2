

<div class="checkout-page">

    <div class="container">


            <h1 class="checkout-heading stylish-heading">Réglement</h1>
            @if (session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
            @endif

            @if(count($errors) > 0)
            <div class="spacer"></div>
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="checkout-section">
                <div class="reglement">
                    
                    <form action="{{ route('checkout.store') }}" method="POST" id="payment-form">
                    <form action="" method="" id="payment-form">
                        @csrf
                        @method('POST')
                        <h2>Détails de la facturation</h2>

                        <div class="form-group">
                            <label for="email">Adresse e-mail</label>
                            @if (auth()->user())
                                <input type="email" class="form-control" id="email" name="email" value="{{ auth()->user()->email }}" readonly>
                            @else
                                <input type="email" class="form-control" id="email" name="email" value="" >
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ auth()->user()->name }}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="address">Adresse</label>
                            <input type="text" class="form-control" id="address" name="address" value="" required >
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="city">Ville</label>
                                <input type="text" class="form-control" id="city" name="city" value="" required >
                            </div>
                            <div class="form-group col-md-6">
                                <label for="province">Pays</label>
                                <input type="text" class="form-control" id="country" name="country" value="" required >
                            </div>
                        </div> <!-- end half-form -->

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="postalcode">Code postal</label>
                                <input type="number" class="form-control" id="postalcode" name="postalcode" value="" required >
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone">Téléphone</label>
                                <input type="tel" class="form-control" id="phone" name="phone" value="" required >
                            </div>
                        </div> <!-- end half-form -->
                    

                        

                        <h2>Détails de paiement</h2>

                        <div class="form-group">
                            <label for="name_on_card">Titulaire de la carte</label>
                            <input type="text" class="form-control" id="name_on_card" name="name_on_card" value="" required>
                        </div>

                        <div class="form-group">
                            <label class='label-card' for="card-element">Carte de crédit</label> 
                            <input type="text" class="form-control" id="card" name="card" value=""  required> 
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="postalcode">Expiration</label>
                                <input type="month" class="form-control" id="expiration" name="expiration" value="" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone">CVC</label>
                                <input type="text" class="form-control" id="cvc" name="cvc" value="" required>
                            </div>
                        </div> <!-- end half-form -->

                       
                        <button type="submit" id="complete-order" class="btn btn-primary btn-lg btn-block">Compléter la commande</button>

                        
                    </form>

                    
                    
                </div>



                <div class="checkout-table-container">
                    <h2>Votre commande</h2>

                    <div class="checkout-table">
                        @foreach (Cart::content() as $item) 
                        <div class="checkout-table-row">
                            <div class="checkout-table-row-left">
                                <img src="{{ asset('storage/img/products/'.$item->model->image) }}" alt="item" class="checkout-table-img">
                                <div class="checkout-item-details">
                                    <div class="checkout-table-item">{{ $item->model->name }}</div>
                                    @foreach($categories as $category)
                                    <div class="checkout-table-description">{{$category->name}}</div>
                                    @endforeach
                                    <div class="checkout-table-price">{{ $item->model->price }} €</div>
                                </div>
                            </div> <!-- end checkout-table -->

                            <div class="checkout-table-row-right">
                                <div class="checkout-table-quantity">{{ $item->qty}}</div>
                            </div>
                        </div> <!-- end checkout-table-row -->
                        @endforeach
                        

                    </div> <!-- end checkout-table -->

                    <div class="checkout-totals">
                        <div class="checkout-totals-left">
                        @if (session()->has('coupon'))
                            Sous-total : <br>
                            
                                Bon de réduction ({{session()->get('coupon')['name'] }}) :
                                <form action="{{ route('coupon.destroy') }}" method="POST" style="display:inline">
                                    @csrf 
                                    @method('DELETE')
                                    <button class="btn btn-secondary btn-sm" type="submit" style="font-size:14px">Retirer</button>
                                </form>
                                <hr>
                                
                            @endif
                        
                          
                            <span class="checkout-totals-total">Total</span>

                        </div>

                        <div class="checkout-totals-right" style="display:inline">
                        @if (session()->has('coupon'))
                        {{ Cart::subtotal()}} €<br>
                        

                            {{ $discount}}€<br>
                            <hr>
                            
                        @endif
                        
                                   
                            <span class="checkout-totals-total">{{ $newTotal }} €</span>

                        </div>

                    </div> <!-- end checkout-totals -->
                    
                        @if (! session()->has('coupon'))
                            <span href="" class="have-code">Code promo ?</span>

                            <div class="have-code-container">

                                <form action="{{ route('coupon.store') }}" method="POST">
                                    @csrf
                                        <input type="text" name="coupon_code" id="coupon_code">
                                        <button type="submit" class="btn btn-outline-secondary">Appliquer</button>
                                </form>

                            </div> <!-- end have-code-container -->
                        @endif
                    
                        
                </div>

            </div> <!-- end checkout-section -->
        </div>




</div>

