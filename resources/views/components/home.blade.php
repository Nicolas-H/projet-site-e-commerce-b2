@if($mode == 'admin')
<div class="col">
  <div class="mx-auto mb-2">
    <a class="btn btn-primary" href="{{route('home.create')}}" role="button">
      Ajouter un jeu
        <i class="fas fa-plus-circle"></i>
    </a>
  </div>
</div>
@endif


@if (session()->has('success_message'))
<div class="alert alert-success">
    {{ session()->get('success_message') }}
</div>
@endif

@if(count($errors) > 0)
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
  <li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif
<div class="category-box">
  <h1 class="category-header">{{$categoryName}}</h1>
  <div class="inner"></div>
</div>

<div class="product-pagination-box">
  
  <div class="product-container">
  @foreach ($homes as $home)


    <div class="product" id="game-card">
      <a href="/shop/{{$home->slug}}">
        <div class="productHeader">
          <img src="{{ asset('storage/img/products/' . $home->image)}}" style="height:200" class="card-img-top">
        </div>

        <div class="card-body">
          <p class="card-text">{{$home->name}}</p>
          <span class="platform logo">{{$home->category}}</span>

          <div class="product-content">
            <div class="price-container">
              <span class="value">{{$home->price}} €</span>
            </div>
          </div>
          <div class="note">
            <span class="fa fa-star checked" style=" color: orange;"></span>
            <span class="fa fa-star checked" style=" color: orange;"></span>
            <span class="fa fa-star checked" style=" color: orange;"></span>
            <span class="fa fa-star checked" style=" color: orange;"></span>
            <span class="fa fa-star checked" style=" color: orange;"></span>
          </div>
        </div>
      </a>
    </div>
    @endforeach

  </div>
  </div>

{{ $homes->links() }}





