<div class="log-page">
    <div class="account">
        <h1>Mon compte</h1>
    </div>
    <form>
        <div class="login">
            <div class="account-email">
                <span><b>E-mail</b></span>
                <input class="input" type="text"/>
            </div>
            <div class="password">
                <span><b>Mot de passe</b></span>
                <input class="input" type="password"/>
            </div>
            <div class="connexion">
            <input class="btn" type="button" value="Connexion">
            </div>
        </div>
    </form>
    <div class="signup">
        <input class="btn" type="button" value="S'inscrire">
    </div>
</div>
