<link href="{{ asset('css/members.main.css') }}" rel="stylesheet">
<link href="{{ asset('css/members.mobile.css') }}" rel="stylesheet">

<main>
  @if (session()->has('success_message'))
  <div class="alert alert-success">
    {{ session()->get('success_message') }}
  </div>
  @endif

  @if(count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div class="header">
    <h1 class="stylish-heading"><i class="fas fa-id-card"></i> Mon profil</h1>
  </div>
  <div class="my-profile">
    <div class="head">
      @if (Auth::user()->is_admin == 0)

      <div class="card-content">
        <div class="card" style="width: 18rem;">
          <a href="{{route('cart.index')}}" class="card-body">
            <h4 class="card-title"><i class="fas fa-eye"></i> Mes commandes</h4>
            <p class="card-text">Voir l'historique de vos achats</p>
          </a>
        </div>
      </div>
      @endif
      <div class="image_outer_container">
        <div class="green_icon"></div>
        <div class="image_inner_container">
          @if($member->image)
          <img src="{{ asset('/storage/img/members/' . $member->image)}}" alt="image">
          @else
          <img src="https://api.adorable.io/avatars/215/abott@adorable.png" />
          @endif
        </div>
      </div>
    </div>
<div class="hello">
    <h3 class="stylish-heading">Bonjour {{$member->name}} {{$member->lastname}}</h3>
  </div>
    <form action="{{route('members.update', ['member' => $member->id])}}" method="POST" enctype="multipart/form-data">
      @method('PUT')
      @csrf
      <label for="name">Nom</label>
      <input id="name" type="text" name="name" value="{{  $member->name }}" placeholder="Nom" required>
      <label for="lastname">Prénom</label>
      <input id="lastname" type="text" name="lastname" value="{{ $member->lastname }}"
        placeholder="Prenom" required>
      <label for="Email">Email</label>
      <input id="email" type="email" name="email" value="{{ $member->email }}" placeholder="Email"
        required>
      <label for="birth">Date de naissance</label>
      <input id="birth" type="date" name="birth" value="{{ $member->birth }}"
        placeholder="Date de naissance" required>
      @if (Auth::user()->is_admin == 0)
      <label for="balance">Solde en €</label>
      <input id="balance" type="number" name="balance" value="{{ $member->balance }}"
        placeholder="Solde" required>
      @endif
      <label for="image">Photo de profil</label>
      <input type="file" class="form-control-file" name="image" id="image" value="{{ $member->image }}">
      <button type="submit" class="btn btn-success">Mettre à jour</button>
    </form>
  </div>
  <div class="link">
    <a href="/shop" >
    <button class="btn btn-primary" style="text-decoration: none">retourner aux achats</button>
  </a>
  </div>
</main>