<link href="{{ asset('css/members.main.css') }}" rel="stylesheet">
<link href="{{ asset('css/members.mobile.css') }}" rel="stylesheet">



<div class="myorders-box">

    <div class="follow mb-3 pagination">
        <a href="/members" style="text-decoration:none">Profil<i class="fas fa-chevron-right"></i></a>  
        <a href="#" style="text-decoration:none">Mes commandes</a>
    </div>

    <h1 class="myorders-header">Mes commandes</h1>
    
    @foreach ($orders as $order)
    <div class="inner-group">
        <div class="order-group orders-page">
            <div class="order-info">
                <div class="box-inner-info">
                    <div class="info1">
                        
                        <div>Commande n°{{ $order->id }}</div>
                        <div>Quantité : {{ $order->billing_total }}</div>
                    </div>
                    <div class="info2">
                        <div><a href="{{ route('orders.show', $order->id) }}">Détail de la commande</a></div>
                    </div>
                </div>
            </div>
            
            
    
                <div class="order-shipment">
                    <div class="box-inner-content">
                        @foreach ($order->products as $product)
                        <div><h3>{{ $product->name }}</h3></div>
                        <span class=platform>{{ $product->categories->name}}</span>
                        <div class="order-img">
                            <img src="{{ asset('/storage/img/products/' . $product->image)}}" alt="image">
                        </div>
                        @endforeach

                        
                    </div>
                </div>
        </div>
    </div>
    @endforeach
</div>



