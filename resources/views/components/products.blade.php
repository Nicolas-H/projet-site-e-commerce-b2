@if (session()->has('success_message'))
<div class="alert alert-success">
  {{ session()->get('success_message') }}
</div>
@endif

@if(count($errors) > 0)
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="product-page">
  <div class="product-img">
    <img src="{{ asset('/storage/img/products/' . $products->image)}}" alt="image">
  </div>
  <div class="box-product">
    <div class="card">
      <h5 class="card-header">{{$products->name}}</h5>
      <div class="card-body">
        <div class="details-product">
          <p class="card-text">
            <b>Synopsis :</b> {{$products->description}}
          </p>
          <b>Sortie :</b> {{$products->releaseDate}}<br>
          <b>Note :</b> 3,5/5<br>

        </div>




        <div class="buyprice">
          <!-- <a href="#" class="btn btn-primary" style="background-color:#ff5400; border-color:#ff5400" ><i class="fas fa-cart-plus"></i> Acheter</a>  -->
          
          <div class="dispo">{!! $stockLevel !!}</div>

          <div class="buypricebox">
            @if ($products->quantity >0)
            <form action="{{ route('cart.store') }}" method="POST">
              @csrf

              <input type="hidden" name="id" value="{{ $products->id}}">
              <input type="hidden" name="name" value="{{ $products->name}}">
              <input type="hidden" name="price" value="{{ $products->price}}">
              <input type="hidden" name="image" value="{{ $products->image}}">
              <button type="submit" class="btn btn-primary" style="background-color:#ff5400; border-color:#ff5400"><i
                  class="fas fa-cart-plus"></i> Acheter</button>
            </form>
            @endif
            <div class="price">
              <b>{{$products->price}} €</b>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="box-review">
      <div class="avis">
        <div class="card review">
          <div class="card-body">
            <h5 class="card-title">Antonio - 5 août 2019</h5>
            <p class="card-text">Code reçu rapidement ! Au top comme toujours !</p>
            <a href="#" class="btn btn-primary">5/5</a>
          </div>
        </div>
      </div>

      <div class="avis">
        <div class="card review">
          <div class="card-body">
            <h5 class="card-title">Joseph - 10 décembre 2015</h5>
            <p class="card-text">Livraison rapide de la part du site, merci Keygames !</p>
            <a href="#" class="btn btn-primary">5/5</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>