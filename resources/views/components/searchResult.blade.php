<link href="{{ asset('css/search.css') }}" rel="stylesheet">
<link href="{{ asset('css/search.mobile.css') }}" rel="stylesheet">

<div class="search">
  @if (session()->has('success_message'))
  <div class="alert alert-success">
    {{ session()->get('success_message') }}
  </div>
  @endif

  @if(count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div class="follow">
    <a href="/shop">Shop <i class="fas fa-chevron-right"></i></a>

    <a href="#">Search</a>
  </div>

  <div class="search-container">
    <h1>Search Result</h1>
    <p><b>{{$searchResult->total()}}</b> result(s) for <b>"{{ request()->input('query')}}"</b></p>
  </div>

  <div class="table-responsive-md">
    <table class="table">
      <thead class="bg-dark text-light">
        <th scope="col">Nom</th>
        <th scope="col">Plateforme</th>
        <th scope="col">Prix</th>
        <th scope="col">Description</th>

      </thead>
      @foreach ($searchResult as $result)

      <tbody>
        <tr>
          <td class="link"><a href="{{route('shop.show', $result->slug)}}">{{$result->name}}</a></td>
          <td>
            {{ $result->categories->name }}
          </td>
          <td> €{{$result->price}}</td>
          <td>{{$result->description}}</td>

        </tr>
      </tbody>
      @endforeach
    </table>

    {{$searchResult->appends(request()->input())->links()}}
  </div>
</div>