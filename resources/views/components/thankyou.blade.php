<div class="thanks">
<div class="thank-you-section">
   <h1>Merci pour <br> Votre commande !</h1>
   <p>Un e-mail de confirmation a été envoyé</p>
</div>
<div class="backhome">
   <a href="/shop">
      <button type="button" class="btn btn-outline-dark">Page d'accueil</button>
   </a>
</div>
</div>