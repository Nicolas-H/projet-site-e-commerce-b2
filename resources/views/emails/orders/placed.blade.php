@component('mail::message')
# Commande reçue

Merci {{ $order->billing_name }} pour votre commande !


**Email de la commande:** {{ $order->billing_email }}

**Total de la commande:** ${{ ($order->billing_total) }}

@foreach ($order->products as $product)
Produit: {{ $product->name }} <br>
Prix: {{ $product->price }} € <br>
Quantité: {{ $product->pivot->quantity }} <br>
@endforeach

Vous pouvez obtenir plus de détails sur votre commande en vous connectant à notre site Web.

@component('mail::button', ['url' => config('app.url'), 'color' => 'green'])
Keygames
@endcomponent

Merci encore de nous avoir choisis.

Cordialement,<br>
Keygames
@endcomponent