@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pauvre gneu 👾</div>

                <div class="card-body">
                    @if (auth()->user()->is_admin == 1)
                    Bienvenue mon seigneur 🧙‍♂️
                    <a href="{{route('admin.index')}}">Admin</a>
                    @elseif (auth()->user()->is_admin == 0)
                    Rejoins le bon côté de la force 🧝‍♂️
                    <a href="{{route('members.index')}}">Member</a>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection