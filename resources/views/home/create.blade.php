@extends('admin.template')

@section('title', 'Créer un jeu')

@section('content')

<form class="ml-3 mr-3" method="POST" action="{{route('home.store')}}">
    @csrf
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="name">Nom du jeu</label>
            <input type="text" name="name" class="form-control" id="name">
        </div>
        <div class="form-group col-md-6">
            <label for="slug">Slug</label>
            <input type="text" name="slug" class="form-control" id="slug">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="price">Prix du jeu</label>
            <input type="text" name="price" class="form-control" id="price">
        </div>
        <div class="form-group col-md-6">
            <label for="category">categorie</label>
            <input type="text" name="category" class="form-control" id="category">
        </div>
    </div>
    <div class="form-group">
        <label for="releaseDate">Date de sortie</label>
        <input type="text" name="releaseDate" class="form-control" id="releaseDate">
    </div>
    <div class="form-group row">
        <label for="description">description</label>
        <textarea type="text" name="description" class="form-control" id="description"></textarea>
    </div>
    <button type="submit" class="btn btn-primary" style="margin-left:43%;">créer</button>
</form>
@endsection