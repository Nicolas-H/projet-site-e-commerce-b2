@extends('admin.template')

@section('title', 'Modifier un jeu')

@section('content')
<div class="jumbotron h-100 w-100 " style="margin:0 auto">
  <form class="ml-3 mr-3" method="POST" action="{{route('home.update', ['home' => $home->id])}}">
    @method('PUT')
    @csrf
    <div class="productHeader" style="text-align:center;">
      <img src="/img/halo5.jpg" style="height: 250px; width:200px" class="card-img-top">
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="name">Nom du jeu</label>
        <input type="text" name="name" class="form-control" id="name" value="{{$home->name}}">
      </div>
      <div class="form-group col-md-6">
        <label for="slug">Slug</label>
        <input type="text" name="slug" class="form-control" id="slug" value="{{$home->slug}}">
      </div>

      <div class="form-group col-md-6">
        <label for="price">Prix du jeu</label>
        <input type="text" name="price" class="form-control" id="price" value="{{$home->price}}">
      </div>
    </div>

    <div class="form-group">
      <label for="description">description</label>
      <textarea type="description" name="description" class="form-control" style="height:100px"
        id="description">{{$home->description}}</textarea>
    </div>
    <div class="form-group col-md-6">
      <label for="releaseDate">Date de sortie</label>
      <input type="text" name="releaseDate" class="form-control" id="releaseDate" value="{{$home->releaseDate}}">
    </div>
    <button type="submit" class="btn btn-primary" style="margin-left:35%;">Mettre à jour</button>
  </form>
</div>
@endsection