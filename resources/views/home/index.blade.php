@extends('layouts.template')
@section('title', 'Accueil')
@section('content')
@component('components.home', ['homes' => $home, 'categories' => $categories,'categoryName' => $categoryName, 'mode' => 'public'])
@endcomponent
@endsection