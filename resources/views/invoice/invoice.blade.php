<link href="{{ asset('css/members.main.css') }}" rel="stylesheet">
<link href="{{ asset('css/members.mobile.css') }}" rel="stylesheet">

<div class="myorders-box">
  <h1 class="myorders-header">Ma commande</h1>


  <div class="inner-group">
    <div class="order-group">
      <div class="order-info">
        <div class="box-inner-info">
          <div class="info1">

            <div>Commande n°{{ $order->id }}</div>
            <div>Effectuée le : {{ $order->created_at }}</div>
            <div>Total : {{ $order->billing_total }} €</div>
          </div>
        </div>
      </div>


      <div class="order-shipment">
        <div class="box-inner-content">
          @foreach ($order->products as $product)
          <table class="table">
            <tbody>
              <tr>
                <td>Name</td>
                <td>{{ $order->user->name }}</td>
              </tr>
              <tr>
                <td>Address</td>
                <td>{{ $order->billing_address }}</td>
              </tr>
              <tr>
                <td>City</td>
                <td>{{ $order->billing_city }}</td>
              </tr>
              <tr>
                <td>Total</td>
                <td>{{ $order->billing_total }} €</td>
              </tr>
            </tbody>
          </table>

          @endforeach
        </div>
      </div>



    </div>
  </div>

  <div class="page-break"></div>
  <style>
    .page-break {
      page-break-after: always;
    }
  </style>

  <div class="inner-group">
    <div class="order-group">
      <div class="order-info">
        <div class="box-inner-info">
          <div class="info1">

            <div>Produits Commandés</div>
          </div>
        </div>
      </div>



      <div class="order-shipment">
        <div class="box-inner-content">
          @foreach ($order->products as $product)
          <div class="order-product-item">
            <div class="slug-item">
              <b><a href="{{ route('shop.show', $product->slug) }}">{{ $product->name }}</a></b>
            </div>
            <div class="order-img">
              <img src="{{ asset('/storage/img/products/' . $product->image)}}" alt="image">
            </div>


            <div class="bottoms-order">
              <p>Total : {{ $product->price }} €</p>
              <p>Quantity : {{ $product->pivot->quantity }}</p>  
          </div>
          </div>
        </div>

        @endforeach
      </div>
    </div>



  </div>
</div>

</div>