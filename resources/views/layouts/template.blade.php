<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mobile.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css"
        integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M" crossorigin="anonymous">

    <!----This link is for the stars rating...--->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <title>@yield('title')</title>
</head>

<body>
    <header class="navbar navbar-light">
        <a class="navbar-brand" href="/">
            <img src="/img/logo.png" width="75" height="70" class="d-inline-block align-top" alt="">
            <i>KEYGAMES</i>
        </a>


        <form action="{{route('search')}}" method="GET" class="form-inline">
            <input class="form-control mr-sm-2" type="text" name="query" id="query"
                value="{{ request()->input('query') }}" placeholder="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
        </form>
    </header>

    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="icon-home">
                <a href="/" class="item"><i class="fas fa-home"></i></a>
            </div>
            <ul class="navbar-nav pl-0">
                @foreach ($categories as $category)
                <li class="nav-item">

                    <a class="nav-item nav-link text-white"
                        href="{{ route('shop.index', ['category' => $category->id ])}}"><span>{{ $category->name }}</span></a>
                </li>
                @endforeach
            </ul>
        </div>

        <div class="connect">
            @if(Auth::check())
            <div class="account-item">
                <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->lastname }}
                        {{ Auth::user()->name }}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item text-dark" href="{{route('members.index')}}">Mon profil</a>
                        @if (Auth::user()->is_admin == 0)
                        <a class="dropdown-item text-dark" href="{{route('orders.index')}}">Mes commandes</a>
                        @elseif (Auth::user()->is_admin == 1)
                        <a class="dropdown-item text-dark" href="{{route('admin.index')}}">Mon dashboard</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-dark" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
            @if (Auth::user()->is_admin == 0)
            <div class="cart-nav">
                <a href="{{ route('cart.index') }}" class="nav-item nav-link text-white">
                    <i class="fas fa-shopping-cart"></i>

                    @if (Cart::instance('default')->count() > 0)
                    <span class="cart-count"><span>{{ Cart::instance('default')->count() }}</span></span>
                    @endif
                </a>
            </div>
            @endif


            @else
            <a class="nav-item nav-link " href="/login">Connexion <i class="fas fa-user-circle"></i></a>
        </div>

        @endif

    </nav>


    <div class="box">
        <div class="jumbotron bg-light">
            @yield('content')

        </div>
    </div>

    <footer>
        <div class="footer-copyright text-center">
            <div class="text">
                <span>
                    © 2020 Copyright:
                    <a href="/"> KEYGAMES.FR </a>
                    <a href="/admin"><i class="fa fa-sign-in"></i></a>
                </span>
                <br>
                <span>
                    Made with ♥ by Nicolas HERMOSILLA & Antonio RAKOTOMALALA
                </span>
            </div>
        </div>
    </footer>

    @yield('extra-js')

</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>

</html>
