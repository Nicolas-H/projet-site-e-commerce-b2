@extends('layouts.template')
@section('title', 'modifier mon profil')
@section('content')
<h5>
    <a href="/admin/memberList" style="text-decoration:none">Liste des membres <i class="fas fa-chevron-right"></i></a>
<a href="#" style="text-decoration:none"> Editer {{$memberList->name}}</a>
</h5>
<div class="jumbotron h-100 w-100 " style="margin:0 auto">
    
    <form class="ml-3 mr-3" method="POST" action="{{route('memberList.update', ['memberList' => $memberList->id])}}"  enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">Nom</label>
                <input type="text" name="name" class="form-control" id="name" value="{{$memberList->name}}">
            </div>
            <div class="form-group col-md-6">
                <label for="lastname">Prénom</label>
                <input type="text" name="lastname" class="form-control" id="lastname" value="{{$memberList->lastname}}">
            </div>
        </div>
            <div class="form-group">
                <label for="email">email</label>
                <input type="text" name="email" class="form-control" id="email" value="{{$memberList->email}}">
            </div>
        <div class="form-group">
            <label for="birth">Date de naissance</label>
            <input type="date" name="birth" id="birth" class="form-control" value="{{ $memberList->birth }}">
        </div>
        <div class="btn w-100 d-flex" style="justify-content:center">
            <button type="submit" class="btn btn-primary">Mettre à
                jour</button>
        </div>
    </form>
</div>
@endsection