@extends('layouts.template')
@section('title', 'modifier mon profil')
@section('content')
<div class="jumbotron h-100 w-100 " style="margin:0 auto">
    <form class="ml-3 mr-3" method="POST" action="{{route('members.update', ['member' => $member->id])}}"
        enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">Nom</label>
                <input type="text" name="name" class="form-control" id="name" value="{{$member->name}}">
            </div>
            <div class="form-group col-md-6">
                <label for="lastname">Prénom</label>
                <input type="text" name="lastname" class="form-control" id="lastname" value="{{$member->lastname}}">
            </div>
        </div>
        <div class="form-group">
            <label for="email">email</label>
            <input type="text" name="email" class="form-control" id="email" value="{{$member->email}}">
        </div>
        <div class="form-group">
            <label for="birth">Date de naissance</label>
            <input type="date" name="birth" id="birth" class="form-control" value="{{ $member->birth }}">
        </div>

        <div class="form-group">
            <label for="balance">Solde</label>
            <input type="text" name="balance" class="form-control" id="balance" value="{{$member->balance}}">
        </div>
        <div class="form-group">
            <div class="form-group">
                <label for="image">Choisissez une nouvelle image</label>
                <input type="file" class="form-control-file" name="image" id="image" value="{{$member->image}}">
            </div>
        </div>
        <div class="btn w-100 d-flex" style="justify-content:center">
            <button type="submit" class="btn btn-primary">Mettre à
                jour</button>
        </div>
    </form>
</div>
@endsection