@extends('layouts.template')
@section('title', 'Accueil')
@section('content')
@component('components.members', ['member' => $member, 'categories' => $categories, 'mode' => 'member'])
@endcomponent
@endsection