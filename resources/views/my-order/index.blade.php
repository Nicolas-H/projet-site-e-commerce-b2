@extends('layouts.template')
@section('title', 'Ma commande')
@section('content')
@component('components.my-order', ['products' => $products, 'order' => $order, 'member' => $member, 'categories' => $categories, 'mode' => 'member'])
@endcomponent
@endsection