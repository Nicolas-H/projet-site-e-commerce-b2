@extends('layouts.template')
@section('title', 'Mes commandes')
@section('content')
@component('components.my-orders', ['orders' => $orders, 'member' => $member, 'categories' => $categories, 'mode' => 'member'])
@endcomponent
@endsection