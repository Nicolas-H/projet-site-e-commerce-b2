@extends('admin.template')

@section('title', 'Créer un jeu')

@section('content')
<h5 class="mb-3">
    <a href="/admin/gamelist" style="text-decoration:none">Liste des jeux <i class="fas fa-chevron-right"></i></a>
<a href="#" style="text-decoration:none"> Créer un jeu</a>
</h5>
<div class="jumbotron h-100 w-100 " style="margin:0 auto">
<form class="form ml-3 mr-3" method="POST" action="{{route('product.store')}}" enctype="multipart/form-data">
    @csrf
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="name">Nom du jeu</label>
            <input type="name" name="name" class="form-control" id="name" required>
        </div>
        <div class="form-group col-md-6">
            <label for="slug">Slug</label>
            <input type="slug" name="slug" class="form-control" id="slug" required>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="price">Prix du jeu</label>
            <input type="price" name="price" class="form-control" id="price" required>
        </div>
        <div class="form-group col-md-4">
            <label for="category_id">Plateforme</label>
            <select class="form-control" id="category_id" name="category_id" required>
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-4">
            <label for="quantity">Quantité</label>
            <input type="number" name="quantity" class="form-control" id="quantity" required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="releaseDate">Date de sortie</label>
            <input type="date" name="releaseDate" class="form-control" id="releaseDate" required>
        </div>
        <div class="form-group col-md-6">
            <label for="featured">En vedette</label>
            <select class="form-control" id="featured" name="featured" required>
                <option value="1">Oui</option>
                <option value="0">Non</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="description">Description</label>
        <textarea type="description" name="description" class="form-control" id="description"></textarea>
    </div>

    <div class="form-group d-flex flex-column">
        <label for="image">Image</label>
        <input type="file" name="image">
    </div>
    
    <button type="submit" class="btn btn-primary">Créer</button>
</form>
</div>
@endsection