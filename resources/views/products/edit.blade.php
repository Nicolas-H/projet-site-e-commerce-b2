@extends('admin.template')

@section('title', 'Modifier un jeu')

@section('content')

<h3>
  <a href="/admin/gamelist" style="text-decoration:none">liste des jeux <i class="fas fa-chevron-right"></i></a>
<a href="#" style="text-decoration:none"> Editer {{$product->name}}</a>
</h3>
<div class="jumbotron h-100 w-100 " style="margin:0 auto">
  <form class="form ml-3 mr-3" method="POST" action="{{route('product.update', ['product' => $product->id])}}" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <!-- <div class="productHeader" style="text-align:center;">
      <img src="/img/halo5.jpg" style="height: 250px; width:200px" class="card-img-top">
    </div> -->
   <div class="form-row">
      <div class="form-group col-md-6">
        <label for="name">Nom du jeu</label>
        <input type="text" name="name" class="form-control" id="name" value="{{$product->name}}">
      </div>
      <div class="form-group col-md-6">
        <label for="slug">Slug</label>
        <input type="text" name="slug" class="form-control" id="slug" value="{{$product->slug}}">
      </div>
   </div>


   <div class="form-row">
      <div class="form-group col-md-4">
        <label for="price">Prix du jeu</label>
        <input type="text" name="price" class="form-control" id="price" value="{{$product->price}}">
      </div>

      <div class="form-group col-md-4">
        <label for="category_id">Plateforme</label>
        <select class="form-control" id="category_id" name="category_id" required>
            @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
      </div>

      <div class="form-group col-md-4">
          <label for="quantity">Quantité</label>
          <input type="number" name="quantity" class="form-control" id="quantity" value="{{$product->quantity}}">
      </div>
   </div>
    

    <div class="form-group">
      <label for="description">description</label>
      <textarea type="text" name="description" class="form-control" style="height:100px"
        id="description">{{$product->description}}</textarea>
    </div>

    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="releaseDate">Date de sortie</label>
        <input type="date" name="releaseDate" class="form-control" id="releaseDate" value="{{$product->releaseDate}}">
      </div>
      <div class="form-group col-md-6">
        <label for="featured">En vedette</label>
        <select class="form-control" id="featured" name="featured">
            <option value="1">Oui</option>
            <option value="0">Non</option>
        </select>
      </div>
    </div>

    <div class="form-group d-flex flex-column">
        <label for="image">Image</label>
    <input type="file" name="image" id="image" value="{{old('$product', $product->image)}}">
    </div>
    
    <button type="submit" class="btn btn-primary" style="margin-left:35%;">Mettre à jour</button>
  </form>
</div>
@endsection