@extends('layouts.template')
@section('title', 'Liste des jeux')
@section('content')
@component('components.products', [ 'products' => $products, 'categories' => $categories, 'stockLevel' => $stockLevel, 'mode' => 'public'])
@endcomponent
@endsection