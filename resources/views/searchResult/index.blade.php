@extends('layouts.template')
@section('title', 'Search Result')
@section('content')
@component('components.searchResult', ['searchResult' => $searchResult, 'categories' => $categories, 'mode' => 'public'])
@endcomponent
@endsection