@extends('layouts.template')
@section('title', 'Merci !')
@section('content')
@component('components.thankyou', [ 'categories' => $categories, 'mode' => 'public'])
@endcomponent
@endsection