<?php

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//  Route::get('/invoice', function() {

//     $pdf = PDF::loadView('pdf.invoice', $data);
//     return $pdf->download('invoice.pdf');

//     // return view('components.invoice');
//  });

// Route::get('/email', function() {
//     Mail::to('keygames@eshop.com')->send(new WelcomeMail());

//     return new WelcomeMail();
// });

Route::get('/', 'HomeController@public');
Route::get('/shop', 'HomeController@public')->name('shop.index');
Route::get('/shop/{product}', 'HomeController@show')->name('shop.show');

Route::get('/login', 'LoginController@public');
Route::get('/myaccount', 'LoginController@account');
Route::get('/search', 'SearchController@search')->name('search');

Auth::routes();
Route::middleware('auth')->group(function () {
    Route::get('/members', 'MemberController@index');
    Route::get('/members/edit', 'MemberController@edit')->name('member.edit');
    Route::get('/cart', 'CartController@public');
    Route::patch('/cart/{product}', 'CartController@update')->name('cart.update');
    Route::delete('/cart/{product}', 'CartController@destroy')->name('cart.destroy');
    Route::get('empty', function () {
        Cart::destroy();
    });
    Route::get('/checkout', 'CheckoutController@index')->name('checkout.index');
    Route::post('/checkout', 'CheckoutController@store')->name('checkout.store');
    Route::post('/coupon', 'CouponController@store')->name('coupon.store');
    Route::delete('/coupon', 'CouponController@destroy')->name('coupon.destroy');
    Route::get('/thankyou', 'ConfirmationController@index')->name('confirmation.index');
    Route::post('/coupon', 'CouponController@store')->name('coupon.store');
    Route::delete('/coupon', 'CouponController@destroy')->name('coupon.destroy');
    Route::get('/my-orders', 'OrdersController@index')->name('orders.index');
    Route::get('/my-orders/{order}', 'OrdersController@show')->name('orders.show');

    Route::resources([
        'members' => 'MemberController',
        '/cart' => 'CartController',
        'memberList' => 'MemberListController',
    ]);
    Route::middleware('admin')->group(function () {
        Route::get('/admin', 'AdminController@index');
        Route::get('admin/gamelist', 'AdminController@gamelist')->name('product.index');
        Route::post('admin/gamelist', 'ProductController@create')->name('product.create');
        Route::post('admin/gamelist', 'ProductController@edit')->name('product.edit');
        Route::post('admin/gamelist', 'ProductController@destroy')->name('product.destroy');
        Route::get('admin/memberList', 'AdminController@memberList')->name('member.index');
        Route::post('admin/memberList', 'MemberListController@edit')->name('memberList.edit');
        Route::post('admin/memberList', 'MemberListController@destroy')->name('memberList.destroy');

        Route::resources([
            'admin' => 'AdminController',
            'admin/product' => 'ProductController',
            'admin/member' => 'MemberController',
            'admin/memberList' => 'MemberListController',
        ]);
    });
});
Route::get('/home', 'HomeController@register')->name('home');

Route::get('/mailable', function () {
    $order = App\Order::find(1);

    return new App\Mail\OrderPlaced($order);
});

Route::get('/invoice/{id}', 'OrdersController@getPdf')->name('getPdf');
// Route::get('invoice', function () {
//     $pdf = PDF::loadview('invoice/invoice');
//     return $pdf->download('invoice.pdf');
// });
